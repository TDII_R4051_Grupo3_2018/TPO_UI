/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *tab_Conectar;
    QPushButton *pushButton_SALIR;
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton_AYUDA;
    QGroupBox *groupBox_2;
    QPushButton *PushButton_ACTUALIZAR;
    QLabel *ESTADO;
    QComboBox *comboBox_PUERTOS;
    QPushButton *PushButton_CONECTAR;
    QWidget *tab_Mantenimiento;
    QPushButton *pushButton_SALIR2;
    QLabel *label_3;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_H;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_H0N;
    QPushButton *pushButton_H0;
    QPushButton *pushButton_H0P;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_V;
    QPushButton *pushButton_V0N;
    QPushButton *pushButton_V0;
    QPushButton *pushButton_V0P;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QWidget *tab_Grafico;
    QPushButton *pushButton_SALIRG;
    QCustomPlot *customPlot;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(681, 593);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 691, 601));
        tabWidget->setStyleSheet(QStringLiteral(""));
        tab_Conectar = new QWidget();
        tab_Conectar->setObjectName(QStringLiteral("tab_Conectar"));
        pushButton_SALIR = new QPushButton(tab_Conectar);
        pushButton_SALIR->setObjectName(QStringLiteral("pushButton_SALIR"));
        pushButton_SALIR->setGeometry(QRect(560, 520, 101, 41));
        pushButton_SALIR->setStyleSheet(QStringLiteral("font: 75 16pt \"MS Shell Dlg 2\";"));
        label = new QLabel(tab_Conectar);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(140, 20, 351, 41));
        label->setStyleSheet(QLatin1String("font: 87 26pt \"Arial Black\";\n"
"color: rgb(0, 0, 255);"));
        label_2 = new QLabel(tab_Conectar);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(0, 0, 681, 571));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/sol.PNG")));
        label_2->setScaledContents(true);
        pushButton_AYUDA = new QPushButton(tab_Conectar);
        pushButton_AYUDA->setObjectName(QStringLiteral("pushButton_AYUDA"));
        pushButton_AYUDA->setGeometry(QRect(560, 400, 101, 111));
        pushButton_AYUDA->setStyleSheet(QLatin1String("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(255, 0, 0, 255), stop:1 rgba(255, 255, 0, 255));\n"
"font: 87 16pt \"Arial Black\";\n"
"color: rgb(255, 255, 255);"));
        groupBox_2 = new QGroupBox(tab_Conectar);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(190, 170, 261, 211));
        groupBox_2->setStyleSheet(QLatin1String("font: 75 16pt \"MS Shell Dlg 2\";\n"
"color: rgb(0, 0, 0);\n"
"color: rgb(0, 0, 255);"));
        PushButton_ACTUALIZAR = new QPushButton(groupBox_2);
        PushButton_ACTUALIZAR->setObjectName(QStringLiteral("PushButton_ACTUALIZAR"));
        PushButton_ACTUALIZAR->setGeometry(QRect(10, 160, 241, 31));
        PushButton_ACTUALIZAR->setStyleSheet(QStringLiteral("color: rgb(0, 0, 0);"));
        ESTADO = new QLabel(groupBox_2);
        ESTADO->setObjectName(QStringLiteral("ESTADO"));
        ESTADO->setGeometry(QRect(50, 30, 161, 41));
        comboBox_PUERTOS = new QComboBox(groupBox_2);
        comboBox_PUERTOS->setObjectName(QStringLiteral("comboBox_PUERTOS"));
        comboBox_PUERTOS->setGeometry(QRect(10, 120, 241, 31));
        comboBox_PUERTOS->setStyleSheet(QStringLiteral("color: rgb(0, 0, 0);"));
        PushButton_CONECTAR = new QPushButton(groupBox_2);
        PushButton_CONECTAR->setObjectName(QStringLiteral("PushButton_CONECTAR"));
        PushButton_CONECTAR->setGeometry(QRect(10, 80, 241, 31));
        PushButton_CONECTAR->setStyleSheet(QStringLiteral("color: rgb(0, 0, 0);"));
        tabWidget->addTab(tab_Conectar, QString());
        label_2->raise();
        pushButton_SALIR->raise();
        label->raise();
        pushButton_AYUDA->raise();
        groupBox_2->raise();
        tab_Mantenimiento = new QWidget();
        tab_Mantenimiento->setObjectName(QStringLiteral("tab_Mantenimiento"));
        pushButton_SALIR2 = new QPushButton(tab_Mantenimiento);
        pushButton_SALIR2->setObjectName(QStringLiteral("pushButton_SALIR2"));
        pushButton_SALIR2->setGeometry(QRect(550, 520, 111, 31));
        label_3 = new QLabel(tab_Mantenimiento);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(0, 0, 681, 571));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/mtto.PNG")));
        label_3->setScaledContents(true);
        layoutWidget = new QWidget(tab_Mantenimiento);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(85, 182, 471, 221));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_H = new QLabel(layoutWidget);
        label_H->setObjectName(QStringLiteral("label_H"));
        label_H->setStyleSheet(QLatin1String("font: 87 12pt \"Arial Black\";\n"
""));
        label_H->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_H);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton_H0N = new QPushButton(layoutWidget);
        pushButton_H0N->setObjectName(QStringLiteral("pushButton_H0N"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_H0N->sizePolicy().hasHeightForWidth());
        pushButton_H0N->setSizePolicy(sizePolicy);
        pushButton_H0N->setStyleSheet(QLatin1String("font: 20pt \"MS Shell Dlg 2\";\n"
"selection-color: rgb(255, 170, 0);"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/izq.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_H0N->setIcon(icon);
        pushButton_H0N->setIconSize(QSize(48, 48));

        horizontalLayout->addWidget(pushButton_H0N);

        pushButton_H0 = new QPushButton(layoutWidget);
        pushButton_H0->setObjectName(QStringLiteral("pushButton_H0"));
        sizePolicy.setHeightForWidth(pushButton_H0->sizePolicy().hasHeightForWidth());
        pushButton_H0->setSizePolicy(sizePolicy);
        pushButton_H0->setStyleSheet(QLatin1String("font: 20pt \"MS Shell Dlg 2\";\n"
"selection-color: rgb(255, 170, 0);"));
        QIcon icon1;
        icon1.addFile(QStringLiteral("zero_h.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_H0->setIcon(icon1);
        pushButton_H0->setIconSize(QSize(48, 48));

        horizontalLayout->addWidget(pushButton_H0);

        pushButton_H0P = new QPushButton(layoutWidget);
        pushButton_H0P->setObjectName(QStringLiteral("pushButton_H0P"));
        sizePolicy.setHeightForWidth(pushButton_H0P->sizePolicy().hasHeightForWidth());
        pushButton_H0P->setSizePolicy(sizePolicy);
        pushButton_H0P->setStyleSheet(QLatin1String("font: 20pt \"MS Shell Dlg 2\";\n"
"selection-color: rgb(255, 170, 0);"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/der.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_H0P->setIcon(icon2);
        pushButton_H0P->setIconSize(QSize(48, 48));

        horizontalLayout->addWidget(pushButton_H0P);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_V = new QLabel(layoutWidget);
        label_V->setObjectName(QStringLiteral("label_V"));
        label_V->setStyleSheet(QStringLiteral("font: 87 12pt \"Arial Black\";"));
        label_V->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_V);

        pushButton_V0N = new QPushButton(layoutWidget);
        pushButton_V0N->setObjectName(QStringLiteral("pushButton_V0N"));
        pushButton_V0N->setStyleSheet(QLatin1String("font: 20pt \"MS Shell Dlg 2\";\n"
"selection-color: rgb(255, 170, 0);"));
        QIcon icon3;
        icon3.addFile(QStringLiteral("arr.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_V0N->setIcon(icon3);
        pushButton_V0N->setIconSize(QSize(48, 48));

        verticalLayout_2->addWidget(pushButton_V0N);

        pushButton_V0 = new QPushButton(layoutWidget);
        pushButton_V0->setObjectName(QStringLiteral("pushButton_V0"));
        pushButton_V0->setStyleSheet(QLatin1String("font: 20pt \"MS Shell Dlg 2\";\n"
"selection-color: rgb(255, 170, 0);"));
        QIcon icon4;
        icon4.addFile(QStringLiteral("zero_v.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_V0->setIcon(icon4);
        pushButton_V0->setIconSize(QSize(48, 48));

        verticalLayout_2->addWidget(pushButton_V0);

        pushButton_V0P = new QPushButton(layoutWidget);
        pushButton_V0P->setObjectName(QStringLiteral("pushButton_V0P"));
        pushButton_V0P->setStyleSheet(QLatin1String("font: 20pt \"MS Shell Dlg 2\";\n"
"selection-color: rgb(255, 170, 0);"));
        QIcon icon5;
        icon5.addFile(QStringLiteral("abj.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_V0P->setIcon(icon5);
        pushButton_V0P->setIconSize(QSize(48, 48));

        verticalLayout_2->addWidget(pushButton_V0P);


        horizontalLayout_2->addLayout(verticalLayout_2);

        radioButton = new QRadioButton(tab_Mantenimiento);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(21, 51, 46, 17));
        radioButton->setChecked(true);
        radioButton_2 = new QRadioButton(tab_Mantenimiento);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(21, 74, 60, 17));
        tabWidget->addTab(tab_Mantenimiento, QString());
        label_3->raise();
        layoutWidget->raise();
        pushButton_SALIR2->raise();
        radioButton_2->raise();
        radioButton->raise();
        tab_Grafico = new QWidget();
        tab_Grafico->setObjectName(QStringLiteral("tab_Grafico"));
        pushButton_SALIRG = new QPushButton(tab_Grafico);
        pushButton_SALIRG->setObjectName(QStringLiteral("pushButton_SALIRG"));
        pushButton_SALIRG->setGeometry(QRect(590, 540, 75, 23));
        customPlot = new QCustomPlot(tab_Grafico);
        customPlot->setObjectName(QStringLiteral("customPlot"));
        customPlot->setGeometry(QRect(10, 10, 661, 511));
        tabWidget->addTab(tab_Grafico, QString());
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Seguidor Solar - Tecnicas Digitales 2", nullptr));
        pushButton_SALIR->setText(QApplication::translate("MainWindow", "SALIR", nullptr));
        label->setText(QApplication::translate("MainWindow", "SEGUIDOR DE LUZ", nullptr));
        label_2->setText(QString());
        pushButton_AYUDA->setText(QApplication::translate("MainWindow", "AYUDA", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "CONEXION", nullptr));
        PushButton_ACTUALIZAR->setText(QApplication::translate("MainWindow", "ACTUALIZAR", nullptr));
        ESTADO->setText(QApplication::translate("MainWindow", "ESTADO", nullptr));
        PushButton_CONECTAR->setText(QApplication::translate("MainWindow", "CONECTAR", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_Conectar), QApplication::translate("MainWindow", "Conexi\303\263n", nullptr));
        pushButton_SALIR2->setText(QApplication::translate("MainWindow", "SALIR", nullptr));
        label_3->setText(QString());
        label_H->setText(QApplication::translate("MainWindow", "SERVO HORIZONTAL", nullptr));
        pushButton_H0N->setText(QString());
        pushButton_H0->setText(QString());
        pushButton_H0P->setText(QString());
        label_V->setText(QApplication::translate("MainWindow", "SERVO VERTICAL", nullptr));
        pushButton_V0N->setText(QString());
        pushButton_V0->setText(QString());
        pushButton_V0P->setText(QString());
        radioButton->setText(QApplication::translate("MainWindow", "Auto", nullptr));
        radioButton_2->setText(QApplication::translate("MainWindow", "Remoto", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_Mantenimiento), QApplication::translate("MainWindow", "Mantenimiento", nullptr));
        pushButton_SALIRG->setText(QApplication::translate("MainWindow", "SALIR", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_Grafico), QApplication::translate("MainWindow", "Gr\303\241ficos", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
