#include "crear_plot.h"
#include "qcustomplot.h"

crear_plot::crear_plot( QCustomPlot *cp, QObject *parent )
    :QObject(parent)
{
    CP=cp;
}

void crear_plot::Nuevo_Grafico()
{
    G = CP->addGraph();
    G->setData(X,Y);
}

void crear_plot::Borrar_Grafico()
{
    CP->removeGraph(G);
}
