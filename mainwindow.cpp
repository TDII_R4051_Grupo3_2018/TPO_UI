#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

#define MAX_LENGHT    100

#define MIN_ADC_Y     1000
#define MAX_ADC_Y     4200

#define sII        1
#define sSI        2
#define sID        3
#define sSD        4


/**********************************************************************************
 * @fn          MainWindow
 * @brief       Constructor de la clase
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    m_init = true;

    ui->setupUi(this);
    puerto=NULL;
    QList<QSerialPortInfo> port= QSerialPortInfo::availablePorts(); //QSerialEnumerator::getPorts();
    ui->comboBox_PUERTOS->clear();
    for(int i=0;i<port.size();i++)
        {
            ui->comboBox_PUERTOS->addItem(port.at(i).QSerialPortInfo::portName());
        }
    setupRealTime ();
    m_init = false;
}


/**********************************************************************************
 * @fn          ~MainWindow
 * @brief       Destructor de la clase
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
}

/**********************************************************************************
 * @fn          on_pushbutton_SALIR_clicked
 * @brief       Botón de la pestaña 'conexión' para salir de la app.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_SALIR_clicked()
{
    QMessageBox::StandardButton salir;

    salir = QMessageBox::question(this, "Salir", "Está seguro que desea salir?", QMessageBox::Yes|QMessageBox::No);

    if ( salir == QMessageBox::Yes )
        this->close();
}


/**********************************************************************************
 * @fn          on_pushbutton_AYUDA_clicked
 * @brief       Botón de la pestaña 'conexión' para solicitar ayuda.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_AYUDA_clicked()
{
    help Ayuda;
    Ayuda.setModal(true);
    Ayuda.exec();
}


/**********************************************************************************
 * @fn          on_pushbutton_CONECTAR_clicked
 * @brief       Botón de la pestaña 'conexión' para conectarse al LPC1769.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_PushButton_CONECTAR_clicked()
{ 

    if(!puerto)
    {
            puerto= new QSerialPort(ui->comboBox_PUERTOS->currentText());
            puerto->setBaudRate(QSerialPort::Baud9600);
            puerto->setDataBits(QSerialPort::Data8);
            puerto->setFlowControl(QSerialPort::NoFlowControl);
            puerto->setStopBits(QSerialPort::OneStop);
            puerto->setParity(QSerialPort::NoParity);

            ui->tab_Mantenimiento->setDisabled(true);

            if(puerto->open(QIODevice::ReadWrite))//==true
            {
                connect(puerto,SIGNAL(readyRead()),this,SLOT(Lectura_uC()));
                ui->ESTADO->setStyleSheet("font-weight: bold; color: black; background-color: red;");
                ui->ESTADO->setText("CONECTADO");
                ui->tab_Mantenimiento->setDisabled(false);
                ui->tab_Grafico->setDisabled(false);

                ui->tabWidget->setCurrentIndex(2);
            }
    }

   else
   {
       puerto->close();
       delete puerto;
       puerto=NULL;

       ui->tab_Mantenimiento->setDisabled(true);
       ui->tab_Grafico->setDisabled(true);

       ui->ESTADO->setStyleSheet("font-weight: bold; color: black; background-color: lightgreen;");
       ui->ESTADO->setText("DESCONECTADO");
   }
}


/**********************************************************************************
 * @fn          on_pushbutton_ACTUALIZAR_clicked
 * @brief       Botón de la pestaña 'conexión' para actualizar puertos.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_PushButton_ACTUALIZAR_clicked()
{
    ui->comboBox_PUERTOS->clear();
    QList <QSerialPortInfo> port = QSerialPortInfo::availablePorts();
    for ( int i=0 ; i<port.size() ; i++ )
        ui->comboBox_PUERTOS->addItem(port.at(i).QSerialPortInfo::portName());
}


/**********************************************************************************
 * @fn          Conectado
 * @brief       Función que verifica la conexión de la app con el microcontrolador.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
bool MainWindow::Conectado()
{
    return puerto && puerto->isOpen();
}


/**********************************************************************************
 * @fn          NoConectadoError
 * @brief       Función que devuelve error en caso de desconexión.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::NoConectadoError()
{
    if (!m_init)
        QMessageBox::warning(this, QString::fromUtf8("Error de conexión"), "No conectado");
}


/**********************************************************************************
 * @fn          on_tabWidget_tabBarclicked
 * @brief       Pestaña 'mantenimiento', comportamiento al ser presionada.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_tabWidget_tabBarClicked(int index)
{
    QByteArray msg = "iE"; //Deshabilita lectura ADC para entrar a "modo mantenimiento". iE:inhibit encendido.
    QMessageBox::StandardButton warning;

    switch (index)
    {
        case 0:
                if (Conectado())
                {
                    warning = QMessageBox::warning(this, "Advertencia", "Su dispositivo ya ha sido conectado.", QMessageBox::Ok);
                    if ( warning == QMessageBox::Ok )
                        ui->tab_Conectar->setDisabled(true);
                }
                else
                    NoConectadoError();
                break;
        case 1:
                if (Conectado())
                    puerto->write(msg);
                else
                    NoConectadoError();
                break;
        case 2:
                if (Conectado())
                {}
//                    puerto->write(msg);
                else
                    NoConectadoError();
                break;
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_SALIR2_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para salir de la app.
 *              Y habilita el panel de nuevo.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_SALIR2_clicked()
{
    QMessageBox::StandardButton salir2;

    salir2 = QMessageBox::question(this, "Salir", "Está seguro que desea salir?", QMessageBox::Yes|QMessageBox::No);

    if ( salir2 == QMessageBox::Yes )
         this->close();
}


/**********************************************************************************
 * @fn          on_pushbutton_H0N_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para mover servo horizontal
 *              sentido antihorario.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_H0N_clicked()
{
    QByteArray msg = "[hI"; //Motor horizontal se mueve hacia izquierda (antihorario).
    if(puerto)
    {
        puerto->write(msg);
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_H0_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para mover servo horizontal
 *              al 0.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_H0_clicked()
{
    QByteArray msg = "[hZ"; //Motor horizontal se mueve a zero (centro).
    if(puerto)
    {
        puerto->write(msg);
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_H0P_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para mover servo horizontal
 *              sentido horario.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_H0P_clicked()
{
    QByteArray msg = "[hD"; //Motor horizontal se mueve hacia derecha (horario).
    if(puerto)
    {
        puerto->write(msg);
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_V0N_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para mover servo vertical
 *              sentido antihorario.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_V0N_clicked()
{
    QByteArray msg = "[vI"; //Motor vertical se mueve hacia izquierda (antihorario).
    if(puerto)
    {
        puerto->write(msg);
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_V0_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para mover servo vertical
 *              al 0.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_V0_clicked()
{
    QByteArray msg = "[vZ"; //Motor vertical se mueve a zero (centro).
    if(puerto)
    {
        puerto->write(msg);
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_V0P_clicked
 * @brief       Botón de la pestaña 'mantenimiento' para mover servo vertical
 *              sentido horario.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_V0P_clicked()
{
    QByteArray msg = "[vD"; //Motor vertical se mueve hacia derecha (antihorario).
    if(puerto)
    {
        puerto->write(msg);
    }
}


/**********************************************************************************
 * @fn          on_pushbutton_SALIRG_clicked
 * @brief       Botón de la pestaña 'gráficos' para salir de la app.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::on_pushButton_SALIRG_clicked()
{
    QMessageBox::StandardButton salir;

    salir = QMessageBox::question(this, "Salir", "Está seguro que desea salir?", QMessageBox::Yes|QMessageBox::No);

    if ( salir == QMessageBox::Yes )
        this->close();
}


/**********************************************************************************
 * @fn          Lectura_uC
 * @brief       Lectura de datos transmitidos por el microcontrolador.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::Lectura_uC()
{
    QByteArray bytes;
    int cant = puerto->bytesAvailable();
    bytes.resize(cant);
    puerto->read(bytes.data(),cant);
    com_Data=bytes;
    ProcesarComandos();
}


/**********************************************************************************
 * @fn          ProcesarComandos
 * @brief       Procesamiento de datos transmitidos por el microcontrolador.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::ProcesarComandos()
{
    typedef enum
    {
        INICIO_DE_TRAMA=0,
        IDENTIFICA_SENSOR,
        VALUE_ADC_1,
        VALUE_ADC_2,
        VALUE_ADC_3,
        VALUE_ADC_4,
        FINAL_DE_TRAMA
    }ESTADOS;

    QString r_ii;
    QString r_si;
    QString r_id;
    QString r_sd;

    static ESTADOS estado = INICIO_DE_TRAMA;
    int flag_id=0;

    for ( int i = 0 ; i < com_Data.count() ; i++ )
    {
        unsigned char dato = com_Data[i];

        switch (estado)
        {
                case INICIO_DE_TRAMA:
                            if ( dato == '<' )
                                estado = IDENTIFICA_SENSOR;
                            else
                                estado = INICIO_DE_TRAMA;
                            break;
                case IDENTIFICA_SENSOR:
                            if ( dato == 'a' || dato == 'b' || dato == 'c' || dato == 'd' )
                            {
                                if ( dato == 'a' )
                                    flag_id = sII;
                                if ( dato == 'b' )
                                    flag_id = sSI;
                                if ( dato == 'c' )
                                    flag_id = sID;
                                if ( dato == 'd' )
                                    flag_id = sSD;
                                estado = VALUE_ADC_1;
                            }
                            else estado = INICIO_DE_TRAMA;
                            break;
                case VALUE_ADC_1:
                            if ( dato >= 48 && dato<= 57 )
                            {
                                    if ( flag_id == sII )
                                        r_ii.prepend(dato);
                                    if ( flag_id == sSI )
                                        r_si.prepend(dato);
                                    if ( flag_id == sID )
                                        r_id.prepend(dato);
                                    if ( flag_id == sSD )
                                        r_sd.prepend(dato);
                                    estado = VALUE_ADC_2;
                            }
                            else
                            {
                                estado = INICIO_DE_TRAMA;
                                r_ii.clear();
                                r_si.clear();
                                r_id.clear();
                                r_sd.clear();
                            }
                            break;
                case VALUE_ADC_2:
                            if ( dato >= 48 && dato<= 57 )
                            {
                                    if ( flag_id == sII )
                                        r_ii.prepend(dato);
                                    if ( flag_id == sSI )
                                        r_si.prepend(dato);
                                    if ( flag_id == sID )
                                        r_id.prepend(dato);
                                    if ( flag_id == sSD )
                                        r_sd.prepend(dato);
                                        estado = VALUE_ADC_3;
                            }
                            else
                            {
                                estado = INICIO_DE_TRAMA;
                                r_ii.clear();
                                r_si.clear();
                                r_id.clear();
                                r_sd.clear();
                            }
                            break;
                case VALUE_ADC_3:
                            if ( dato >= 48 && dato<= 57 )
                            {
                                    if ( flag_id == sII )
                                        r_ii.prepend(dato);
                                    if ( flag_id == sSI )
                                        r_si.prepend(dato);
                                    if ( flag_id == sID )
                                        r_id.prepend(dato);
                                    if ( flag_id == sSD )
                                        r_sd.prepend(dato);
                                    estado = VALUE_ADC_4;
                            }
                            else
                            {
                                estado = INICIO_DE_TRAMA;
                                r_ii.clear();
                                r_si.clear();
                                r_id.clear();
                                r_sd.clear();
                            }
                            break;
                case VALUE_ADC_4:
                            if ( dato >= 48 && dato<= 57 )
                            {
                                    if ( flag_id == sII )
                                        r_ii.prepend(dato);
                                    if ( flag_id == sSI )
                                        r_si.prepend(dato);
                                    if ( flag_id == sID )
                                        r_id.prepend(dato);
                                    if ( flag_id == sSD )
                                        r_sd.prepend(dato);
                                    estado = FINAL_DE_TRAMA;
                            }
                            else
                            {
                                estado = INICIO_DE_TRAMA;
                                r_ii.clear();
                                r_si.clear();
                                r_id.clear();
                                r_sd.clear();
                            }
                            break;
                case FINAL_DE_TRAMA:
                            if ( dato == '>' )
                            {
                                if ( r_ii != NULL )
                                    II = r_ii.toLocal8Bit();
                                if ( r_si != NULL )
                                    SI = r_si.toLocal8Bit();
                                if ( r_id != NULL )
                                    ID = r_id.toLocal8Bit();
                                if ( r_sd != NULL )
                                    SD = r_sd.toLocal8Bit();
                            }
                            estado = INICIO_DE_TRAMA;
                            r_ii.clear();
                            r_si.clear();
                            r_id.clear();
                            r_sd.clear();
                            break;
        }
    }
}


/**********************************************************************************
 * @fn          setupRealTime
 * @brief       Procesamiento de datos transmitidos por el microcontrolador.
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::setupRealTime ()
{
  ui->customPlot->addGraph(); // blue line
  ui->customPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
  ui->customPlot->graph(0)->setName("Sensor Inferior Izquierdo");
  ui->customPlot->addGraph(); // red line
  ui->customPlot->graph(1)->setPen(QPen(QColor(255, 0, 40)));
  ui->customPlot->graph(1)->setName("Sensor Superior Izquierdo");
  ui->customPlot->addGraph(); // green line
  ui->customPlot->graph(2)->setPen(QPen(QColor(0, 255, 0)));
  ui->customPlot->graph(2)->setName("Sensor Inferior Derecho");
  ui->customPlot->addGraph(); // black line
  ui->customPlot->graph(3)->setPen(QPen(QColor(0, 0, 0)));
  ui->customPlot->graph(3)->setName("Sensor Superior Derecho");

  ui->customPlot->plotLayout()->insertRow(0);
  ui->customPlot->plotLayout()->addElement(0, 0, new QCPTextElement(ui->customPlot, "COMPORTAMIENTO DE LOS LDR", QFont("sans", 12, QFont::Bold)));

  ui->customPlot->legend->setVisible(true);

  QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  timeTicker->setTimeFormat("%h:%m:%s");
  ui->customPlot->xAxis->setTicker(timeTicker);
  ui->customPlot->axisRect()->setupFullAxesBox();
  ui->customPlot->yAxis->setRange(MIN_ADC_Y, MAX_ADC_Y);

  ui->customPlot->xAxis->setLabel("TIEMPO");
  ui->customPlot->yAxis->setLabel("LECTURA ADC");

  // make left and bottom axes transfer their ranges to right and top axes:
  connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
  connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));

  // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
  connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
  dataTimer.start(0); // Interval 0 means to refresh as fast as possible
}


/**********************************************************************************
 * @fn          realtimeDataSlot
 * @brief       Ploteo de lecturas de ADC
 *
 * @param[in]   void
 * @return      void
 * ********************************************************************************/
void MainWindow::realtimeDataSlot()
{
  static QTime time(QTime::currentTime());
  // calculate two new data points:
  double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
  static double lastPointKey = 0;

  double ii, si, id, sd;
  ii = II.toDouble();
  si = SI.toDouble();
  id = ID.toDouble();
  sd = SD.toDouble();

  if (key-lastPointKey > 0.002) // at most add point every 2 ms
  {
    // add data to lines:
    ui->customPlot->graph(0)->addData(key, ii);
    ui->customPlot->graph(1)->addData(key, si);
    ui->customPlot->graph(2)->addData(key, id);
    ui->customPlot->graph(3)->addData(key, sd);

    lastPointKey = key;
  }
  // make key axis range scroll with the data (at a constant range size of 8):
  ui->customPlot->xAxis->setRange(key, 8, Qt::AlignRight);
  ui->customPlot->replot();

  // calculate frames per second:
  static double lastFpsKey;
  static int frameCount;
  ++frameCount;
  if (key-lastFpsKey > 2) // average fps over 2 seconds
  {
       lastFpsKey = key;
       frameCount = 0;
  }
}


void MainWindow::on_radioButton_clicked()
{
    QByteArray msg = "A"; //Colocacion en modo Automatico
    if(puerto)
    {
        puerto->write(msg);
    }
}

void MainWindow::on_radioButton_2_clicked()
{
    QByteArray msg = "R"; //Colocacion en modo Remoto
    if(puerto)
    {
        puerto->write(msg);
    }
}
