#ifndef CREAR_PLOT_H
#define CREAR_PLOT_H

#include <QVector>
#include <QObject>

class QCPGraph;
class QCustomPlot;

class crear_plot : public QObject
{
public:
    crear_plot(QCustomPlot *cp, QObject *parent=nullptr );

    void Nuevo_Grafico();
    void Borrar_Grafico();

    void setX (const QVector<double> &x)
    {
        X=x;
    }

    void setY (const QVector<double> &y)
    {
        Y=y;
    }

private:
    QCPGraph *G;
    QVector<double>X;
    QVector<double>Y;
    QCustomPlot *CP;
};

#endif // CREAR_PLOT_H
