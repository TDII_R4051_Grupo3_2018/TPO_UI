#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T14:20:03
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = seguidor_solar
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    help.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    help.h \
    qcustomplot.h

FORMS    += mainwindow.ui \
    help.ui \    

RESOURCES += \
    rscimagenes.qrc
