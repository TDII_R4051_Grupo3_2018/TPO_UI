#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QDateTimeEdit"
#include "QListWidget"
#include "QPlainTextEdit"
#include "qserialport.h"
#include "qserialportinfo.h"
#include "qcustomplot.h"
#include "qvector.h"
#include <QVector>
#include <QMessageBox>
#include <QLayout>
#include "help.h"
#include "crear_plot.h"
#include <QSharedPointer>
#include <QTime>
#include <iostream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QSerialPort *Mainwindow(QSerialPort *p){p = puerto; return p;}

    QSerialPort *puerto;
    QDateTime rtc;
    bool m_init;
    QByteArray com_Data;
    QByteArray II;
    QByteArray SI;
    QByteArray ID;
    QByteArray SD;

    //QVector<double> II;
    //QVector<double> SI;
    //QVector<double> ID;
    //QVector<double> SD;

    void SetRTC(QDateTime horafecha, bool update = true);
    void UpdateRTC();
    bool Conectado();
    void NoConectadoError();
    void ProcesarComandos ();
    void setupRealTime();


private slots:
    void Lectura_uC();
    void realtimeDataSlot();
    void on_tabWidget_tabBarClicked(int index);

    void on_PushButton_CONECTAR_clicked();
    void on_PushButton_ACTUALIZAR_clicked();
    void on_pushButton_AYUDA_clicked();
    void on_pushButton_SALIR_clicked();

    void on_radioButton_clicked();
    void on_radioButton_2_clicked();
    void on_pushButton_H0N_clicked();
    void on_pushButton_H0_clicked();
    void on_pushButton_H0P_clicked();
    void on_pushButton_V0N_clicked();
    void on_pushButton_V0_clicked();
    void on_pushButton_V0P_clicked();
    void on_pushButton_SALIR2_clicked();

    void on_pushButton_SALIRG_clicked();

private:
    Ui::MainWindow *ui;

    QTimer dataTimer;
};

#endif // MAINWINDOW_H
