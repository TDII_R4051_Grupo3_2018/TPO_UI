#include "help.h"
#include "ui_help.h"

help::help(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::help)
{
    ui->setupUi(this);
}

help::~help()
{
    delete ui;
}

void help::on_pushButton_clicked()
{
    QMessageBox::StandardButton ok;

    ok = QMessageBox::question(this, "Ayuda", "Está seguro que desea salir del cuadro de ayuda?", QMessageBox::Yes|QMessageBox::No);

    if ( ok == QMessageBox::Yes )
        this->close();
}
