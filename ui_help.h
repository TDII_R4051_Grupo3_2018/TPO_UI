/********************************************************************************
** Form generated from reading UI file 'help.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HELP_H
#define UI_HELP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_help
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_6;
    QLabel *label_7;
    QPushButton *pushButton;

    void setupUi(QDialog *help)
    {
        if (help->objectName().isEmpty())
            help->setObjectName(QStringLiteral("help"));
        help->resize(339, 385);
        verticalLayout_2 = new QVBoxLayout(help);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(help);
        label->setObjectName(QStringLiteral("label"));
        label->setStyleSheet(QLatin1String("font: 87 10pt \"Arial Black\";\n"
"color: rgb(255, 0, 0);"));
        label->setScaledContents(false);
        label->setWordWrap(true);

        verticalLayout->addWidget(label);

        label_2 = new QLabel(help);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setStyleSheet(QLatin1String("font: 87 10pt \"Arial Black\";\n"
"color: rgb(0, 170, 0);"));
        label_2->setScaledContents(false);
        label_2->setWordWrap(true);

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(help);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setStyleSheet(QLatin1String("font: 87 10pt \"Arial Black\";\n"
"color: rgb(170, 85, 255);"));
        label_3->setScaledContents(false);
        label_3->setWordWrap(true);

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(help);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setStyleSheet(QLatin1String("font: 87 10pt \"Arial Black\";\n"
"color: rgb(0, 170, 0);"));
        label_4->setScaledContents(false);
        label_4->setWordWrap(true);

        verticalLayout->addWidget(label_4);

        label_6 = new QLabel(help);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setStyleSheet(QLatin1String("font: 87 10pt \"Arial Black\";\n"
"color: rgb(170, 85, 255);"));
        label_6->setScaledContents(false);
        label_6->setWordWrap(true);

        verticalLayout->addWidget(label_6);

        label_7 = new QLabel(help);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font;
        font.setFamily(QStringLiteral("Arial Black"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(10);
        label_7->setFont(font);
        label_7->setStyleSheet(QLatin1String("font: 87 10pt \"Arial Black\";\n"
"color: rgb(0, 170, 255);"));
        label_7->setScaledContents(false);
        label_7->setWordWrap(true);

        verticalLayout->addWidget(label_7);


        verticalLayout_2->addLayout(verticalLayout);

        pushButton = new QPushButton(help);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout_2->addWidget(pushButton);

        label->raise();
        label_2->raise();
        label_3->raise();
        label_4->raise();
        label_6->raise();
        label_7->raise();
        pushButton->raise();

        retranslateUi(help);

        QMetaObject::connectSlotsByName(help);
    } // setupUi

    void retranslateUi(QDialog *help)
    {
        help->setWindowTitle(QApplication::translate("help", "Ayuda", nullptr));
        label->setText(QApplication::translate("help", "Presionar bot\303\263n \"CONECTAR\" si en la lista desplegables hay un puerto disponible.", nullptr));
        label_2->setText(QApplication::translate("help", "Si no hay ning\303\272n puerto disponible, verificar lo conexi\303\263n entre el kit Infotronic y la computadora y presionar el bot\303\263n \"ACTUALIZAR\".", nullptr));
        label_3->setText(QApplication::translate("help", "Si la conexi\303\263n fue exitosa aparecer\303\241 en ventana un label rojo con la leyenda \"CONECTADO\". Ahora puede ir a otra pesta\303\261a o configurar reloj.", nullptr));
        label_4->setText(QApplication::translate("help", "\"Mantenimiento\": puede manipular los servomotores manualmente.", nullptr));
        label_6->setText(QApplication::translate("help", "\"Gr\303\241ficos\": se observa el comportamiento de cada uno de los sensores.", nullptr));
        label_7->setText(QApplication::translate("help", "Ingresar valor comprendido entre 1 y 99 al lado de \"AJUSTAR LA SENSIBILIDAD\" y presionar enviar para variar comportamiento de panel.", nullptr));
        pushButton->setText(QApplication::translate("help", "SALIR", nullptr));
    } // retranslateUi

};

namespace Ui {
    class help: public Ui_help {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HELP_H
